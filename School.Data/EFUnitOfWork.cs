﻿using System;
using System.Linq;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace School.Data
{
	public class EFUnitOfWork : IUnitOfWork
	{
		private DataContext _context;

		public EFUnitOfWork(DataContext context)
		{
			_context = context;
		}

		public DataContext Context => _context;

		public ITransaction BeginTransaction()
		{
			return new DbTransaction(_context.Database.BeginTransaction());
		}

		public void Add<T>(T obj)
				where T : class
		{
			var set = _context.Set<T>();
			set.Add(obj);
		}

		public void Update<T>(T obj)
				where T : class
		{
			var set = _context.Set<T>();
			set.Attach(obj);
			_context.Entry(obj).State = EntityState.Modified;
		}

		void IUnitOfWork.Remove<T>(T obj)
		{
			var set = _context.Set<T>();
			set.Remove(obj);
		}

		public IQueryable<T> Query<T>()
				where T : class
		{
			return _context.Set<T>();
		}

		public void Commit()
		{
			_context.SaveChanges();
		}

		public async Task CommitAsync()
		{
			await _context.SaveChangesAsync();
		}

		public void Attach<T>(T obj) where T : class
		{
			var set = _context.Set<T>();
			set.Attach(obj);
		}

		public void Dispose()
		{
			_context = null;
		}
	}
}
