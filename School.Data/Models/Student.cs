﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace School.Data.Models
{
	public class Student
	{
		[Key]
		public int ID { get; set; }

		[Required, ForeignKey("Class")]
		public int ClassID { get; set; }
		public virtual Class Class { get; set; }

		[Required, StringLength(50)]
		public string Name { get; set; }

		[Required, StringLength(50)]
		public string Family { get; set; }

		[Required]
		public int Age { get; set; }

		[Required]
		public float GPA { get; set; }
	}
}
