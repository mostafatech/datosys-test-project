﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace School.Data
{
	public class DataContext : DbContext
	{
		DataContextOptions options;

		public DataContext()
		{
		}
		public DataContext(DataContextOptions options)
		{
			this.options = options;
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (options == null)
			{
				// for migrations
				optionsBuilder.UseSqlServer("Server=.;Database=School;Trusted_Connection=True;");
			}
			else
			{
				if (options.DatabaseType == DataContextDatabaseType.SqlServer)
				{
					optionsBuilder.UseSqlServer(options.DatabaseConnectionString);
				}
				else
				{
					optionsBuilder.UseInMemoryDatabase(options.DatabaseConnectionString);
				}
			}

			base.OnConfiguring(optionsBuilder);
		}

		//entities
		public DbSet<Models.Class> Classes { get; set; }
		public DbSet<Models.Student> Students { get; set; }
	}
}
