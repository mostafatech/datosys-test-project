﻿using System;
using System.Collections.Generic;

namespace School.Data
{
	public interface ITransaction : IDisposable
	{
		void Commit();
		void Rollback();
	}
}
