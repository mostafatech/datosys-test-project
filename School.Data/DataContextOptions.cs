﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Data
{
	public class DataContextOptions
	{
		public DataContextDatabaseType DatabaseType { get; set; }
		public string DatabaseConnectionString { get; set; }
	}

	public enum DataContextDatabaseType
	{
		SqlServer,
		InMemory
	}
}
