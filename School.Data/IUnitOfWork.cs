﻿using System;
using System.Linq;
using System.Transactions;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace School.Data
{
	public interface IUnitOfWork : IDisposable
	{
		ITransaction BeginTransaction();

		void Add<T>(T obj) where T : class;
		void Update<T>(T obj) where T : class;
		void Remove<T>(T obj) where T : class;
		IQueryable<T> Query<T>() where T : class;
		void Commit();
		Task CommitAsync();
		void Attach<T>(T obj) where T : class;
	}
}
