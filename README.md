# datosys-test-project

> Master-Detail sample project by Asp.net Core and vuejs

## Build Setup
This application uses VueJS as js-framework and Scss as css pre-processor. You must build client side before run the application from Visual Studio.

``` bash
# goto ui direcory
cd School.UI

# install dependencies
npm install

# build css and javascript files
npm run build

# build and serve on port 63058
Hit Run on Visual Studio
or
dotnet run
```