﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Library
{
	public interface IApplicationLogger
	{
		void Information(string message);
		void Debug(string message);
		void Error(string message);
		void Error(Exception ex);
	}
}
