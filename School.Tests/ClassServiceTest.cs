﻿using System;
using System.Linq;
using Xunit;

namespace School.Tests
{
	public class ClassServiceTest
	{
		Services.ClassService classService;

		public ClassServiceTest()
		{
			classService = Helpers.Dependencies.GetClassService();
		}

		[Fact]
		public void AddClassShouldWork()
		{
			var aClass = new Services.DTOs.CreateClass
			{
				Name = "AddClassShouldWork_Class",
				Location = "Testing Project",
				TeacherName = "Tester"
			};

			var createClassServiceResult = classService.CreateClass(aClass);
			if (createClassServiceResult.Success)
			{
				var findClassByNameServiceResult = classService.FindClassByName("AddClassShouldWork_Class");
				if (findClassByNameServiceResult.Success && findClassByNameServiceResult.Result != null)
				{
					Assert.NotNull(findClassByNameServiceResult.Result);
				}
			}
		}

		[Fact]
		public void GetClassesShouldWork()
		{
			// create a class
			classService.CreateClass(new Services.DTOs.CreateClass
			{
				Name = "GetClassesShouldWork_Class",
				Location = "Testing Project",
				TeacherName = "Tester"
			});

			// get all classes with service
			var getAllClassesServiceResult = classService.GetAllClasses();
			if (getAllClassesServiceResult.Success)
			{
				var foundCreatedClass = getAllClassesServiceResult.Result.Where(x => x.Name == "GetClassesShouldWork_Class").Count();
				Assert.Equal(1, foundCreatedClass);
			}
		}

		[Fact]
		public void UpdateClassShouldWork()
		{
			var findClassByNameServiceResult = classService.FindClassByName("AddClassShouldWork_Class");
			if (findClassByNameServiceResult.Success && findClassByNameServiceResult.Result != null)
			{
				findClassByNameServiceResult.Result.TeacherName = "Tester In School";
				var updateResult = classService.UpdateClass(findClassByNameServiceResult.Result);
				if (updateResult.Success)
				{
					var findClassByNameServiceResult2 = classService.FindClassByName("AddClassShouldWork_Class");
					if (findClassByNameServiceResult2.Success && findClassByNameServiceResult2.Result != null)
					{
						Assert.Equal("Tester In School", findClassByNameServiceResult2.Result.TeacherName);
					}
				}
			}
		}

		[Fact]
		public void DeleteClassShouldWork()
		{
			var findClassByNameServiceResult = classService.FindClassByName("AddClassShouldWork_Class");
			if (findClassByNameServiceResult.Success && findClassByNameServiceResult.Result != null)
			{
				var deleteResult = classService.DeleteClass(findClassByNameServiceResult.Result.ID);
				if (deleteResult.Success)
				{
					var findClassByNameServiceResult2 = classService.FindClassByName("AddClassShouldWork_Class");
					if (findClassByNameServiceResult2.Success && findClassByNameServiceResult2.Result != null)
					{
						Assert.Null(findClassByNameServiceResult2.Result);
					}
				}
			}
		}

	}
}
