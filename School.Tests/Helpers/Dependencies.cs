﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Tests.Helpers
{
	static class Dependencies
	{

		public static Data.IUnitOfWork GetUnitOfWork()
		{
			var options = new Data.DataContextOptions
			{
				DatabaseType = Data.DataContextDatabaseType.InMemory,
				DatabaseConnectionString = "school.inmemorydb"
			};
			var context = new Data.DataContext(options);
			var uow = new Data.EFUnitOfWork(context);
			return uow;
		}

		public static Library.IApplicationLogger GetLogger()
		{
			return new Helpers.NullLogger();
		}

		public static Services.ClassService GetClassService()
		{
			var logger = GetLogger();
			var unitOfWork = GetUnitOfWork();
			var service = new Services.ClassService(logger, unitOfWork);
			return service;
		}

		public static Services.StudentService GetStudentService()
		{
			var logger = GetLogger();
			var unitOfWork = GetUnitOfWork();
			var service = new Services.StudentService(logger, unitOfWork);
			return service;
		}

	}
}
