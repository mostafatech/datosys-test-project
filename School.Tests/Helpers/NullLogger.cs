﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Tests.Helpers
{
	class NullLogger : Library.IApplicationLogger
	{
		public void Debug(string message)
		{
		}

		public void Error(string message)
		{
		}

		public void Error(Exception ex)
		{
		}

		public void Information(string message)
		{
		}
	}
}
