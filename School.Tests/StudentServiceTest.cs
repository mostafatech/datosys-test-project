﻿using System;
using System.Linq;
using Xunit;

namespace School.Tests
{
	public class StudentServiceTest
	{
		Services.ClassService classService;
		Services.StudentService studentService;

		public StudentServiceTest()
		{
			classService = Helpers.Dependencies.GetClassService();
			studentService = Helpers.Dependencies.GetStudentService();
		}

		[Fact]
		public void AddStudentShoudWork()
		{
			var classModel = _getOrCreateDefaultClass();
			var student = new Services.DTOs.CreateStudent
			{
				ClassID = classModel.ID,
				Name = "AddStudentShoudWork_Name",
				Family = "AddStudentShoudWork_Family",
				Age = 0,
				GPA = 0
			};
			var studentCreatedResult = studentService.CreateStudent(student);
			if (studentCreatedResult.Success)
			{
				var findStudentByFullNameAndClassNameResult = studentService.FindStudentByFullNameAndClass("AddStudentShoudWork_Name AddStudentShoudWork_Family", classModel.Name);
				Assert.NotNull(findStudentByFullNameAndClassNameResult.Result);
			}
		}

		[Fact]
		public void GetAllStudentInClassShoudWork()
		{
			// add classes
			var createClassResult1 = classService.CreateClass(new Services.DTOs.CreateClass
			{
				Name = "GetAllStudentInClassShoudWork_Class1",
				Location = "Testing Project",
				TeacherName = "Tester"
			});
			var createClassResult2 = classService.CreateClass(new Services.DTOs.CreateClass
			{
				Name = "GetAllStudentInClassShoudWork_Class2",
				Location = "Testing Project",
				TeacherName = "Tester"
			});

			// ass students
			var createdStudentResult1 = studentService.CreateStudent(new Services.DTOs.CreateStudent
			{
				ClassID = createClassResult1.Result.ID,
				Name = "GetAllStudentInClassShoudWork_StudentName1",
				Family = "GetAllStudentInClassShoudWork_StudentFamily1",
				Age = 20,
				GPA = 0
			});
			var createdStudentResult2 = studentService.CreateStudent(new Services.DTOs.CreateStudent
			{
				ClassID = createClassResult1.Result.ID,
				Name = "GetAllStudentInClassShoudWork_StudentName2",
				Family = "GetAllStudentInClassShoudWork_StudentFamily2",
				Age = 20,
				GPA = 0
			});
			var createdStudentResult3 = studentService.CreateStudent(new Services.DTOs.CreateStudent
			{
				ClassID = createClassResult2.Result.ID,
				Name = "GetAllStudentInClassShoudWork_StudentName3",
				Family = "GetAllStudentInClassShoudWork_StudentFamily3",
				Age = 20,
				GPA = 0
			});

			// get students in classes count
			var countOfClass1 = studentService.GetStudentsInClass(createClassResult1.Result.ID).Result.Count() == 2;
			var countOfClass2 = studentService.GetStudentsInClass(createClassResult2.Result.ID).Result.Count() == 1;
			Assert.True(countOfClass1 && countOfClass2);
		}

		[Fact]
		public void UpdateStudentShouldWork()
		{
			var classModel = _getOrCreateDefaultClass();
			var student = new Services.DTOs.CreateStudent
			{
				ClassID = classModel.ID,
				Name = "UpdateStudentShouldWork_Name",
				Family = "UpdateStudentShouldWork_Family",
				Age = 0,
				GPA = 0
			};
			var studentCreatedResult = studentService.CreateStudent(student);
			if (studentCreatedResult.Success)
			{
				studentCreatedResult.Result.Name = "UpdateStudentShouldWork_Name_Updated";
				studentCreatedResult.Result.Family = "UpdateStudentShouldWork_Family_Updated";
				var updateStudentResult = studentService.UpdateStudent(studentCreatedResult.Result);
				if (updateStudentResult.Success)
				{
					var findStudentByFullNameAndClassNameResult = studentService.FindStudentByFullNameAndClass("UpdateStudentShouldWork_Name_Updated UpdateStudentShouldWork_Family_Updated", classModel.Name);
					Assert.NotNull(findStudentByFullNameAndClassNameResult.Result);
				}
			}
		}

		[Fact]
		public void DeleteStudentShouldWork()
		{
			var classModel = _getOrCreateDefaultClass();
			var student = new Services.DTOs.CreateStudent
			{
				ClassID = classModel.ID,
				Name = "DeleteStudentShouldWork_Name",
				Family = "DeleteStudentShouldWork_Family",
				Age = 0,
				GPA = 0
			};
			var studentCreatedResult = studentService.CreateStudent(student);
			if (studentCreatedResult.Success)
			{
				var deleteStudentResult = studentService.DeleteStudent(studentCreatedResult.Result.ID);
				if (deleteStudentResult.Success)
				{
					var findStudentByFullNameAndClassNameResult = studentService.FindStudentByFullNameAndClass("DeleteStudentShouldWork_Name DeleteStudentShouldWork_Family", classModel.Name);
					Assert.Null(findStudentByFullNameAndClassNameResult.Result);
				}
			}
		}


		private Services.DTOs.ClassDetail _getOrCreateDefaultClass()
		{
			var findClassResult = classService.FindClassByName("ClassForStudentTest");
			if (findClassResult.Result == null)
			{
				var createClassResult = classService.CreateClass(new Services.DTOs.CreateClass
				{
					Name = "ClassForStudentTest",
					Location = "Testing Project",
					TeacherName = "Tester"
				});
				if (createClassResult.Success)
					return createClassResult.Result;
			}

			return findClassResult.Result;
		}

	}
}
