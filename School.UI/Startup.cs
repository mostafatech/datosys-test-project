using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StructureMap;

namespace School.UI
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public IServiceProvider ConfigureServices(IServiceCollection services)
    {
      services.AddMvc();

      // register dependencies
      return ConfigureIoC(services);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseBrowserLink();
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Home/Error");
      }

      app.UseStaticFiles();

      app.UseMvc(routes =>
      {
        routes.MapRoute(
          name: "default",
          template: "{controller=Home}/{action=Index}/{id?}");
      });
    }

    public IServiceProvider ConfigureIoC(IServiceCollection services)
    {
      var container = new Container();

      container.Configure(config =>
      {
        // Register stuff in container, using the StructureMap APIs...
        config.Scan(_ =>
        {
          _.AssemblyContainingType(typeof(Startup));
          _.WithDefaultConventions();
        });

        // inject logger dependency
        var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        config.For<Library.IApplicationLogger>().Use(_ => new ApplicationLogger(logger)).ContainerScoped();

        // inject data and business dependencies
        config.For<Data.DataContext>().Use(_ =>
          new Data.DataContext(new Data.DataContextOptions
          {
            DatabaseType = Data.DataContextDatabaseType.SqlServer,
            DatabaseConnectionString = Configuration.GetConnectionString("Database") //@"Server=.;Database=School;Trusted_Connection=True;"
          })).ContainerScoped();
        config.For(typeof(Data.IUnitOfWork)).Add(typeof(Data.EFUnitOfWork));
        config.For(typeof(Services.ClassService)).Add(typeof(Services.ClassService));
        config.For(typeof(Services.StudentService)).Add(typeof(Services.StudentService));

        //Populate the container using the service collection
        config.Populate(services);
      });

      return container.GetInstance<IServiceProvider>();
    }
  }

  public class ApplicationLogger : Library.IApplicationLogger
  {
    private NLog.Logger logger;
    public ApplicationLogger(NLog.Logger logger)
    {
      this.logger = logger;
    }

    public void Information(string message)
    {
      logger.Info(message);
    }

    public void Debug(string message)
    {
      logger.Debug(message);
    }

    public void Error(string message)
    {
      logger.Error(message);
    }
    public void Error(Exception ex)
    {
      logger.Error(ex);
    }
  }
}
