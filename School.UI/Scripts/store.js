import Vue from 'vue';
import Vuex from 'vuex';
import ApiService from './Services/ApiService'
import ClassesService from './Services/ClassesService'
import StudentsService from './Services/StudentsService'

Vue.use(Vuex);

export default new Vuex.Store({
  // Making sure that we're doing
  // everything correctly by enabling
  // strict mode in the dev environment.
  strict: process.env.NODE_ENV !== 'production',

  state: {
    selectedClass: null,
    classes: [
      //{ ID: 1, Name: 'Biology', Location: 'Building 5 Room 201', Teacher: 'Mr Robertson' },
      //{ ID: 2, Name: 'English', Location: 'Building 3 Room 134', Teacher: 'Miss Sanderson' },
    ],
    students: [
      //{ ID: 1, ClassID: 1, Name: 'David Jackson', Age: 19, GPA: 3.4 },
      //{ ID: 2, ClassID: 1, Name: 'Peter Parker', Age: 19, GPA: 2.9 },
      //{ ID: 3, ClassID: 2, Name: 'Robert Smith', Age: 18, GPA: 3.1 },
      //{ ID: 4, ClassID: 2, Name: 'Rebecca Black', Age: 19, GPA: 2.1 },
    ]
  },

  getters: {
    classes(state) {
      return state.classes.map(v => ({ ...v }));
    },
    students(state) {
      if (state.selectedClass) {
        return state.students.map(v => ({ ...v }));
      }
      return [];
    }
  },

  actions: {
    // classes
    loadClasses({ commit }) {
      return ClassesService.GetAll()
        .then(result => {
          commit('setClasses', result);
        });
    },
    addClass({ commit, dispatch }, model) {
      return ClassesService.Create(model)
        .then(result => {
          dispatch('loadClasses');
        });
    },
    updateClass({ commit, dispatch }, model) {
      return ClassesService.Update(model)
        .then(result => {
          dispatch('loadClasses');
        });
    },
    deleteClass({ commit, dispatch }, model) {
      return ClassesService.Delete(model)
        .then(result => {
          dispatch('loadClasses');
        });
    },

    // students
    loadStudentsInClass({ commit, state }) {
      return StudentsService.GetAllInClass(state.selectedClass.id)
        .then(result => {
          commit('setStudents', result);
        });
    },
    addStudent({ commit, dispatch, state }, model) {
      model.classID = state.selectedClass.id;
      return StudentsService.Create(model)
        .then(result => {
          dispatch('loadStudentsInClass');
        });
    },
    updateStudent({ commit, dispatch }, model) {
      return StudentsService.Update(model)
        .then(result => {
          dispatch('loadStudentsInClass');
        });
    },
    deleteStudent({ commit, dispatch }, model) {
      return StudentsService.Delete(model)
        .then(result => {
          dispatch('loadStudentsInClass');
        });
    },


    changeSelectedClass({ commit, dispatch }, classModel) {
      commit('setSelectedClass', classModel);
      dispatch('loadStudentsInClass');
    }
  },

  mutations: {
    setClasses(state, data) {
      state.classes = data;
    },
    setStudents(state, data) {
      state.students = data;
    },
    setSelectedClass(state, data) {
      state.selectedClass = data;
    }
  }

});
