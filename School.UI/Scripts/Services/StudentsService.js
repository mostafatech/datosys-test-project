import ApiService from './ApiService'

const StudentsService = {

  GetAllInClass(classID) {
    return new Promise((resolve, reject) => {
      ApiService.get('/api/students/' + String(classID)).then(r => {
        if (r.data.success) {
          resolve(r.data.data);
        } else {
          reject(r.data.message);
        }
      })
    })
  },

  Create(model) {
    return new Promise((resolve, reject) => {
      ApiService.post('/api/students', model).then(r => {
        if (r.data.success) {
          resolve(r.data.data);
        } else {
          reject(r.data.message);
        }
      })
    })
  },

  Update(model) {
    return new Promise((resolve, reject) => {
      ApiService.put('/api/students/' + String(model.id), model).then(r => {
        if (r.data.success) {
          resolve(r.data.data);
        } else {
          reject(r.data.message);
        }
      })
    })
  },

  Delete(model) {
    return new Promise((resolve, reject) => {
      ApiService.delete('/api/students/' + String(model.id)).then(r => {
        if (r.data.success) {
          resolve(r.data.data);
        } else {
          reject(r.data.message);
        }
      })
    })
  }

}

export default StudentsService
