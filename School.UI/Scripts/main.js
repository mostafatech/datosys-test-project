import Vue from 'vue'
import store from './store'
import Home from './Home.vue'


new Vue({
  el: '#app',
  store: store,
  render: h => h(Home)
})
