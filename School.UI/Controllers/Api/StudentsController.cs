using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace School.UI.Controllers.Api
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  public class StudentsController : Controller
  {
    private readonly Library.IApplicationLogger logger;
    Services.StudentService studentService;

    public StudentsController(
      Library.IApplicationLogger logger,
      Services.StudentService studentService)
    {
      this.logger = logger;
      this.studentService = studentService;
    }


    // GET: api/students
    [HttpGet("{id}")]
    public Models.ApiResult<IEnumerable<Services.DTOs.StudentDetail>> Get(int id)
    {
      var data = studentService.GetStudentsInClass(classID: id);
      return new Models.ApiResult<IEnumerable<Services.DTOs.StudentDetail>>
      {
        data = data.Result,
        success = true
      };
    }

    // POST: api/students
    [HttpPost]
    public Models.ApiResult Post([FromBody]Services.DTOs.CreateStudent model)
    {
      var serviceResult = studentService.CreateStudent(model);
      if (serviceResult.Success) logger.Information("An Student inserted");
      return new Models.ApiResult { success = serviceResult.Success, message = serviceResult.Message };
    }

    // PUT: api/students/5
    [HttpPut("{id}")]
    public Models.ApiResult Put(int id, [FromBody]Services.DTOs.StudentDetail model)
    {
      model.ID = id;
      var serviceResult = studentService.UpdateStudent(model);
      if (serviceResult.Success) logger.Information("An Student updated");
      return new Models.ApiResult { success = serviceResult.Success, message = serviceResult.Message };
    }

    // DELETE: api/students/5
    [HttpDelete("{id}")]
    public Models.ApiResult Delete(int id)
    {
      var serviceResult = studentService.DeleteStudent(id);
      if (serviceResult.Success) logger.Information("An Student deleted");
      return new Models.ApiResult { success = serviceResult.Success, message = serviceResult.Message };
    }
  }
}
