using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace School.UI.Controllers.Api
{
  [Produces("application/json")]
  [Route("api/[controller]")]
  public class ClassesController : Controller
  {
    private readonly Library.IApplicationLogger logger;
    private Services.ClassService classService;

    public ClassesController(
      Library.IApplicationLogger logger,
      Services.ClassService classService)
    {
      this.logger = logger;
      this.classService = classService;
    }
    

    // GET: api/classes
    [HttpGet]
    public Models.ApiResult<IEnumerable<Services.DTOs.ClassDetail>> Get()
    {
      var data = classService.GetAllClasses();
      return new Models.ApiResult<IEnumerable<Services.DTOs.ClassDetail>>
      {
        data = data.Result,
        success = true
      };
    }

    // POST: api/classes
    [HttpPost]
    public Models.ApiResult Post([FromBody]Services.DTOs.CreateClass model)
    {
      var serviceResult = classService.CreateClass(model);
      if (serviceResult.Success) logger.Information("A Class inserted");
      return new Models.ApiResult { success = serviceResult.Success, message = serviceResult.Message };
    }

    // PUT: api/classes/5
    [HttpPut("{id}")]
    public Models.ApiResult Put(int id, [FromBody]Services.DTOs.ClassDetail model)
    {
      model.ID = id;
      var serviceResult = classService.UpdateClass(model);
      if (serviceResult.Success) logger.Information("A Class updated");
      return new Models.ApiResult { success = serviceResult.Success, message = serviceResult.Message };
    }

    // DELETE: api/classes/5
    [HttpDelete("{id}")]
    public Models.ApiResult Delete(int id)
    {
      var serviceResult = classService.DeleteClass(id);
      if (serviceResult.Success) logger.Information("A Class deleted");
      return new Models.ApiResult { success = serviceResult.Success, message = serviceResult.Message };
    }
  }
}
