using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using School.UI.Models;

namespace School.UI.Controllers
{
  public class HomeController : Controller
  {
    private readonly Library.IApplicationLogger logger;

    public HomeController(
      Library.IApplicationLogger logger)
    {
      this.logger = logger;
    }

    public IActionResult Index()
    {
      return View();
    }

    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}
