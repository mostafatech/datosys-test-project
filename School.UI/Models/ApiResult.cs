using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.UI.Models
{
  public class ApiResult
  {
    public bool success { get; set; }
    public string message { get; set; }
  }

  public class ApiResult<T> : ApiResult
  {
    public T data { get; set; }
  }
}
