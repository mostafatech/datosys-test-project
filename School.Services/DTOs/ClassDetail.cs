﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Services.DTOs
{
	public class ClassDetail
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Location { get; set; }
		public string TeacherName { get; set; }
	}
}
