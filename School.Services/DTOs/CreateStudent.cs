﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Services.DTOs
{
	public class CreateStudent
	{
		public int ClassID { get; set; }
		public string Name { get; set; }
		public string Family { get; set; }
		public int Age { get; set; }
		public float GPA { get; set; }
	}
}
