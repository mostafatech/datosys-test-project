﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Services.DTOs
{
	public class ServiceResult
	{
		public bool Success { get; set; } = true;
		public string Message { get; set; }

		public void Successful(string message = "")
		{
			Success = true;
			Message = message;
		}

		public void Failed(string message = "")
		{
			Success = false;
			Message = message;
		}

		public void Failed(Exception ex)
		{
			Failed(ex.Message);
		}
	}

	public class ServiceResult<T> : ServiceResult
	{
		public T Result { get; set; }

		public void Successful(T result = default(T), string message = "")
		{
			Success = true;
			Message = message;
			Result = result;
		}
	}
}
