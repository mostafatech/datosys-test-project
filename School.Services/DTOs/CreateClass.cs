﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Services.DTOs
{
	public class CreateClass
	{
		public string Name { get; set; }
		public string Location { get; set; }
		public string TeacherName { get; set; }
	}
}
