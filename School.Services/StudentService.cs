﻿using System;
using System.Linq;
using System.Collections.Generic;
using School.Services.DTOs;

namespace School.Services
{
	public class StudentService
	{
		private readonly Library.IApplicationLogger logger;
		private Data.IUnitOfWork _unitOfWork;

		public StudentService(
			Library.IApplicationLogger logger,
			Data.IUnitOfWork _unitOfWork)
		{
			this.logger = logger;
			this._unitOfWork = _unitOfWork;
		}

		public ServiceResult<IEnumerable<StudentDetail>> GetStudentsInClass(int classID)
		{
			var result = new ServiceResult<IEnumerable<StudentDetail>>();

			// validations
			if (classID <= 0) result.Failed("Valid ClassID is reuired");
			if (!result.Success) return result;

			try
			{
				var data = _unitOfWork.Query<Data.Models.Student>()
					.Where(x => x.ClassID == classID)
					.OrderBy(x => x.Name)
					.Select(x => new StudentDetail
					{
						ID = x.ID,
						ClassID = x.ClassID,
						Name = x.Name,
						Family = x.Family,
						Age = x.Age,
						GPA = x.GPA
					}).ToList();

				result.Successful(data);
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

		public ServiceResult<StudentDetail> CreateStudent(CreateStudent dto)
		{
			var result = new ServiceResult<StudentDetail>();

			// validations
			if (dto == null) result.Failed("Invalid parameters");
			else
			{
				if (string.IsNullOrWhiteSpace(dto.Name)) result.Failed("Name is required");
				if (string.IsNullOrWhiteSpace(dto.Family)) result.Failed("Family is required");
				if (dto.Age <= 0) result.Failed("Age is not valid");
				if (dto.GPA < 0) result.Failed("GPA is not valid");
				if (_unitOfWork.Query<Data.Models.Student>().Where(x => x.Family == dto.Family).Any()) result.Failed("Sorry, An Student with same family name already exists");
			}
			if (!result.Success) return result;

			try
			{
				var dataModel = new Data.Models.Student
				{
					ClassID = dto.ClassID,
					Name = dto.Name,
					Family = dto.Family,
					Age = dto.Age,
					GPA = dto.GPA
				};

				_unitOfWork.Add(dataModel);
				_unitOfWork.Commit();

				result.Successful(new StudentDetail
				{
					ID = dataModel.ID,
					ClassID = dataModel.ClassID,
					Name = dataModel.Name,
					Family = dataModel.Family,
					Age = dataModel.Age,
					GPA = dataModel.GPA
				});
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

		public ServiceResult UpdateStudent(StudentDetail dto)
		{
			var result = new ServiceResult();

			// validations
			if (dto == null) result.Failed("Invalid parameters");
			else
			{
				if (string.IsNullOrWhiteSpace(dto.Name)) result.Failed("Name is required");
				if (string.IsNullOrWhiteSpace(dto.Family)) result.Failed("Family is required");
				if (_unitOfWork.Query<Data.Models.Student>().Where(x => x.Family == dto.Family && x.ID != dto.ID).Any()) result.Failed("Sorry, An Student with same family name already exists");
			}
			if (!result.Success) return result;

			try
			{
				var dataModel = new Data.Models.Student
				{
					ID = dto.ID,
					ClassID = dto.ClassID,
					Name = dto.Name,
					Family = dto.Family,
					Age = dto.Age,
					GPA = dto.GPA
				};

				_unitOfWork.Update(dataModel);
				_unitOfWork.Commit();

				result.Successful();
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

		public ServiceResult DeleteStudent(int id)
		{
			var result = new ServiceResult();

			// validations
			if (id <= 0) result.Failed("Valid ID is reuired");
			if (!result.Success) return result;

			try
			{
				var dataModel = new Data.Models.Student { ID = id };

				_unitOfWork.Remove(dataModel);
				_unitOfWork.Commit();

				result.Successful();
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

		public ServiceResult<StudentDetail> FindStudentByFullNameAndClass(string studentFullName, string className)
		{
			var result = new ServiceResult<StudentDetail>();

			try
			{
				var data = _unitOfWork.Query<Data.Models.Student>()
					.Where(x => x.Name + " " + x.Family == studentFullName && x.Class.Name == className)
					.Select(x => new StudentDetail
					{
						ID = x.ID,
						ClassID = x.ClassID,
						Name = x.Name,
						Family = x.Family,
						Age = x.Age,
						GPA = x.GPA
					}).FirstOrDefault();

				result.Successful(data);
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

	}
}
