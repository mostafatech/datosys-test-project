﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace School.Services
{
	public class ClassService
	{
		private readonly Library.IApplicationLogger logger;
		private Data.IUnitOfWork _unitOfWork;

		public ClassService(
			Library.IApplicationLogger logger,
			Data.IUnitOfWork _unitOfWork)
		{
			this.logger = logger;
			this._unitOfWork = _unitOfWork;
		}

		public DTOs.ServiceResult<IEnumerable<DTOs.ClassDetail>> GetAllClasses()
		{
			var result = new DTOs.ServiceResult<IEnumerable<DTOs.ClassDetail>>();
			try
			{
				var data = _unitOfWork.Query<Data.Models.Class>()
					.OrderBy(x => x.Name)
					.Select(x => new DTOs.ClassDetail
					{
						ID = x.ID,
						Name = x.Name,
						Location = x.Location,
						TeacherName = x.TeacherName
					}).ToList();

				result.Successful(data);
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result; ;
		}

		public DTOs.ServiceResult<DTOs.ClassDetail> CreateClass(DTOs.CreateClass dto)
		{
			var result = new DTOs.ServiceResult<DTOs.ClassDetail>();

			// validation
			if (dto == null) result.Failed("Invalid parameters");
			else
			{
				if (string.IsNullOrWhiteSpace(dto.Name)) result.Failed("Name is required");
				if (string.IsNullOrWhiteSpace(dto.Location)) result.Failed("Location is required");
				if (string.IsNullOrWhiteSpace(dto.TeacherName)) result.Failed("Teacher Name is required");
			}
			if (!result.Success) return result;

			try
			{
				var dataModel = new Data.Models.Class
				{
					Name = dto.Name,
					Location = dto.Location,
					TeacherName = dto.TeacherName
				};

				_unitOfWork.Add(dataModel);
				_unitOfWork.Commit();

				result.Successful(new DTOs.ClassDetail
				{
					ID = dataModel.ID,
					Name = dataModel.Name,
					Location = dataModel.Location,
					TeacherName = dataModel.TeacherName
				});
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

		public DTOs.ServiceResult UpdateClass(DTOs.ClassDetail dto)
		{
			var result = new DTOs.ServiceResult();

			// validation
			if (dto == null) result.Failed("Invalid parameters");
			else
			{
				if (string.IsNullOrWhiteSpace(dto.Name)) result.Failed("Name is required");
				if (string.IsNullOrWhiteSpace(dto.TeacherName)) result.Failed("Teacher Name is required");
			}
			if (!result.Success) return result;

			try
			{
				var dataModel = new Data.Models.Class
				{
					ID = dto.ID,
					Name = dto.Name,
					Location = dto.Location,
					TeacherName = dto.TeacherName
				};

				_unitOfWork.Update(dataModel);
				_unitOfWork.Commit();

				result.Successful();
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

		public DTOs.ServiceResult DeleteClass(int id)
		{
			var result = new DTOs.ServiceResult();

			// validation
			if (id <= 0) result.Failed("Valid ID is reuired");
			if (!result.Success) return result;

			try
			{
				var dataModel = new Data.Models.Class { ID = id };
				_unitOfWork.Remove(dataModel);
				_unitOfWork.Commit();

				result.Successful();
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}

		public DTOs.ServiceResult<DTOs.ClassDetail> FindClassByName(string name)
		{
			var result = new DTOs.ServiceResult<DTOs.ClassDetail>();

			try
			{
				var data = _unitOfWork.Query<Data.Models.Class>()
					.Where(x => x.Name == name)
					.Select(x => new DTOs.ClassDetail
					{
						ID = x.ID,
						Name = x.Name,
						Location = x.Location,
						TeacherName = x.TeacherName
					}).FirstOrDefault();

				result.Successful(data);
			}
			catch (Exception ex)
			{
				logger.Error(ex);
				result.Failed(ex);
			}

			return result;
		}
	}
}
